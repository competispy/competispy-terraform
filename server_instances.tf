resource "hcloud_server" "control_planes" {
  count = var.servers_num
  name  = "k3s-control-plane-${count.index}"

  image       = data.hcloud_image.ubuntu.name
  server_type = "cx21"

  ssh_keys = [hcloud_ssh_key.default.id]
  labels = {
    provisioner = "terraform",
    engine      = "k3s",
    node_type   = "control-plane"
  }

  user_data = data.template_file.argcd_yaml.rendered

}

data "template_file" "argcd_yaml" {
  template = file("${path.module}/manifests/cloud-init/init.yaml.tpl")

  vars = {
    content = file("${path.module}/manifests/argocd/GENERATED.yaml")
  }
}

resource "hcloud_server_network" "control_planes" {
  count     = var.servers_num
  subnet_id = hcloud_network_subnet.k3s_nodes.id
  server_id = hcloud_server.control_planes[count.index].id
}
