terraform {
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/28735283/terraform/state/production"
    lock_address   = "https://gitlab.com/api/v4/projects/28735283/terraform/state/production/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/28735283/terraform/state/production/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = "5"
  }

  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "~> 1.28"
    }

    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "~> 2.4"
    }

  }
  required_version = ">= 0.13"
}

provider "hcloud" {
}

provider "kubernetes" {
  host                   = module.k3s.kubernetes.api_endpoint
  cluster_ca_certificate = module.k3s.kubernetes.cluster_ca_certificate
  client_certificate     = module.k3s.kubernetes.client_certificate
  client_key             = module.k3s.kubernetes.client_key
}

