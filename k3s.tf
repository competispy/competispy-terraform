module "k3s" {
  source = "xunleii/k3s/module"

  depends_on_ = [hcloud_server.agents, hcloud_server.control_planes]
  k3s_version = "v1.22.5+k3s1"

  servers = {
    for i in range(length(hcloud_server.control_planes)) :
    hcloud_server.control_planes[i].name => {
      ip = hcloud_server_network.control_planes[i].ip
      connection = {
        host = hcloud_server.control_planes[i].ipv4_address
      }
      flags       = [
        "--disable-cloud-controller",
        "--tls-san k3s.competispy.com",
        "--tls-san ${hcloud_server.control_planes[i].ipv4_address}",
        "--write-kubeconfig-mode 644",
        "--kube-apiserver-arg=oidc-issuer-url=https://gitlab.com",
        "--kube-apiserver-arg=oidc-client-id=8c01806551aa99dd728fcf4c72a949e24511a159c10b1e8bd4454f531cd1c27c",
        "--kube-apiserver-arg=oidc-username-claim=email",
        "--kube-apiserver-arg=oidc-username-prefix=oidc:",
        "--kube-apiserver-arg=oidc-groups-claim=groups_direct"
      ]
      annotations = { "server_id" : i } // theses annotations will not be managed by this module
    }
  }

  agents = {
    for i in range(length(hcloud_server.agents)) :
    "${hcloud_server.agents[i].name}_node" => {
      name = hcloud_server.agents[i].name
      ip   = hcloud_server_network.agents_network[i].ip
      connection = {
        host = hcloud_server.agents[i].ipv4_address
      }
    }
  }
}