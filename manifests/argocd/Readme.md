# ArgoCD Installation files

In here you will find the files related to the initial installation 
of argocd into the cluster and the consequent bootstrap of the cluster from 
the ground up.

The file [GENERATED.yaml](GENERATED.yaml) is a result of running the following
command on the current directory.
```shell
kustomize build --enable-managedby-label . -o GENERATED.yaml
```
> **WARNING**: Please do not manually edit this file. if you need to do changes
> do so in the kustomize configuration and the re-generate the file.

## Updating ArgoCD
One common procedure to do is to upgrade the argocd installation to the latest
available release. To do so go into the [kustomization.yaml](kustomization.yaml)
file and modify the remote url of the argo repository to reflect the latest version
that you want.
