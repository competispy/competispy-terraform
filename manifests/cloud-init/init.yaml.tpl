#cloud-config

# Write ArgoCD install files
write_files:
  - encoding: gzip+base64
    content: ${base64gzip("${content}")}
    path: /var/lib/rancher/k3s/server/manifests/argo-cd.yaml
