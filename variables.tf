variable "ssh_publik_key_file" {
  description = "Path to SSH public Key content needed to provision the instances."
  type        = string
  default     = "~/.ssh/id_rsa.pub"
}

variable "servers_num" {
  description = "Number of control plane nodes."
  default     = 1
}

variable "agents_num" {
  description = "Number of agent nodes."
  default     = 1
}

variable "argocd_namespace" {
  description = "Kubernetes space to use for argocd deployment."
  type        = string
  default     = "argocd"
}

