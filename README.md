# competispy-terraform

Terraform initialization for competispy

## How to get started locally

We are using gitlab to store the terraform state file. This is a good and cheap way of storing this crucial file in a
secure and backup up location.

> **WARNING** This file is readable by any member of the project which means is crucial no secrets are managed by 
> terraform. This is anyway a best practice scenario, so we should embrace it. In general prefer public key 
> authentication over password base.

This fact implies that in order to modify our infrastructure and in consequence the state file you need to be able to 
contact gitlab and the broader internet. This should not be a big deal since you need that to access the cloud providers
api.

### Initialize terraform 

In order to lock the state file (necessary to modify it) you need to be at least a 
[maintainer on the project](https://docs.gitlab.com/ee/user/infrastructure/terraform_state.html#permissions-for-using-terraform).
You can always run `terraform plan -lock=false` after you initialize if you only have dev permissions. This will give 
you a broad idea of what your MR would do.

Create a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) with the api scope
and run the following script.

```shell
terraform init \
    -backend-config="username=<YOUR-USERNAME>" \
    -backend-config="password=<YOUR-ACCESS-TOKEN>"
```

If you can't be bothered with inputting your token and username every time you init the project you can also define the 
following environment variables to your shell and terraform will pick them up.

```shell
export TF_HTTP_USERNAME="<YOUR-USERNAME>"
export TF_HTTP_PASSWORD="<YOUR-ACCESS-TOKEN>"
```

## How to get started with gitlab CI

> **WIP** In the future most operations should be handled by gitlab ci automatically.
> Local terraform applications should be minimized.
